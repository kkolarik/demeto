"""demeto URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import include

from demeto import views, settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('registration/', views.UserCreateView.as_view(), name='register'),
    path('logout/', views.LogOutView.as_view(), name='logout'),
    path('profile/<int:pk>', views.ProfileView.as_view(), name='profile'),
    path('decision/add', views.DecisionAddView.as_view(), name='add-decision'),
    path('decision/alternatives/<int:pk>', views.DecisionAlternativesView.as_view(), name='alter-decision'),
    path('decision/criteria/<int:pk>', views.DecisionCriteriaView.as_view(), name='crit-decision'),
    path('decision/parameters/<int:pk>', views.DecisionParametersView.as_view(), name='param-decision'),
    path('decision/users/<int:pk>', views.DecisionUsersView.as_view(), name='users-decision'),
    path('decision/evaluation/<int:pk>', views.DecisionEvalView.as_view(), name='eval-decision'),
    path('decision/<int:pk>', views.DecisionDetailView.as_view(), name='decision'),
    path('evaluation/add', views.ajaxAddEvaluation, name='eval-add'),
    path('evaluation/check', views.ajaxCheckVotesEvaluation, name='eval-check'),
    re_path('^', include('django.contrib.auth.urls')),
    re_path(r'^$', views.Dashboard.as_view(), name='dashboard'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
