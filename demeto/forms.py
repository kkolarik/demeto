from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm, CharField, Textarea, modelformset_factory, TextInput, \
    PasswordInput, Select, CheckboxInput
from demeto.models import CustomUser, DecisionProject, DecisionUser, \
    DecisionCriterion_Variant


class CustomUserForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'username', 'email', 'image')

class CustomUserEditForm(ModelForm):
    password0 = CharField(label="Old Password", required=False, widget = PasswordInput)
    password1 = CharField(label="New Password", required=False, widget = PasswordInput)
    password2 = CharField(label="New Password confirmation", widget=PasswordInput, required=False)

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name', 'username', 'email', 'image', 'password0', 'password1', 'password2')

    def clean(self):
        cleaned_data = super(CustomUserEditForm, self).clean()
        user = self.instance
        old_password = cleaned_data['password0']
        if old_password:
            if user.check_password(old_password):
                password1 = cleaned_data['password1']
                password2 = cleaned_data['password2']
                if password1 and password2:
                    if password1 != password2:
                        self.add_error('password2', 'The two password fields must match.')
                    else:
                        user.set_password(password1)
                else:
                    self.add_error('password1', 'New password fields are missing.')
            else:
                self.add_error('password0', 'Password does not match current password.')

        return cleaned_data


class DecisionProjectForm(ModelForm):
    description = CharField(widget=Textarea(attrs={'rows': 4, }))

    class Meta:
        model = DecisionProject
        fields = ('name', 'description', 'image')


AlternativesFormSet = modelformset_factory( DecisionCriterion_Variant,
                                            can_delete=True,
                                            fields=('name', 'description', 'image'),
                                            extra=0,
                                            min_num=2,
                                            widgets={'name': TextInput(attrs={'class': 'form-control'}),
                                                     'description': TextInput(attrs={'class': 'form-control'})})

CriteriaFormSet = modelformset_factory( DecisionCriterion_Variant,
                                        can_delete=True,
                                        fields=('name', 'description', 'is_computable', 'computed_value', 'image'),
                                        extra=0,
                                        min_num=2,
                                        widgets={'name': TextInput(attrs={'class': 'form-control'}),
                                                 'is_computable': CheckboxInput(attrs={'class': 'form-check-input'}),
                                                 'computed_value': Select(attrs={'class': 'form-control'}),
                                                 'description': TextInput(attrs={'class': 'form-control'})})

UsersFormSet = modelformset_factory(DecisionUser,
                                    can_delete=True,
                                    fields=('user', 'weight', 'state'),
                                    extra=1,
                                    min_num=1,)