from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView

from demeto.models import DecisionProject, DecisionUser


class RestrictionTemplateMixin:
    def dispatch(self, request, *args, **kwargs):
        decision_id = kwargs['pk']
        get_object_or_404(DecisionProject, pk=decision_id)
        if not DecisionUser.objects.filter(user=self.request.user, decision_id=decision_id).exists():
            return HttpResponseRedirect(reverse('dashboard'))
        state = DecisionProject.objects.get(pk=decision_id).state
        if self.necessary_state > state:
           # TODO get redirect url based on necessary state
           return HttpResponseRedirect(reverse('alter-decision', args=[decision_id]))
        return super().dispatch(request, *args, **kwargs)


class CreateFormsetView(LoginRequiredMixin, RestrictionTemplateMixin, CreateView):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        project = DecisionProject.objects.get(pk=self.kwargs['pk'])
        context['creator_id'] = project.creator_id
        context['state'] = project.state
        return context

    def get(self, request, *args, **kwargs):
        self.object = None

    def post(self, request, *args, **kwargs):
        self.object = None

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))