import re
import warnings

import numpy
import itertools
from django.contrib.auth.models import AbstractUser
from django.db.models import Model, CharField, ForeignKey, DateField, DO_NOTHING, IntegerField, FloatField, \
    BooleanField, CASCADE
from django.db.models.fields.files import ImageField
from django.utils.timezone import now
from io import StringIO


class CustomUser(AbstractUser):
    image = ImageField(null=True, blank=True, upload_to="avatars", default = 'no-image.jpg')


class DecisionProject(Model):
    NEW = 0
    STAGE_ALTERNATIVES = 1
    STAGE_CRITERIA = 2
    STAGE_USERS = 3
    STAGE_EVALUATION = 4
    COMPLETED = 5
    STATE_CHOICES = (
        (NEW, u'New'),
        (STAGE_ALTERNATIVES, u'Added alternatives'),
        (STAGE_CRITERIA, u'Added criteria'),
        (STAGE_USERS, u'Added users'),
        (STAGE_EVALUATION, u'Evaluate criteria'),
        (COMPLETED, u'Completed'),
    )
    MATRIX_PRECISION = 0.000001

    name = CharField(max_length=200)
    description = CharField(max_length=2000, blank=True)
    image = ImageField(null=True, blank=True, upload_to="img/project", default='no-image.jpg')
    state = IntegerField(choices=STATE_CHOICES, default=NEW)
    creator = ForeignKey('CustomUser', on_delete=DO_NOTHING)
    created = DateField(default=now)
    evalMatrix = CharField(max_length=10000, null=True, blank=True)
    evalResult = CharField(max_length=2000, null=True, blank=True)
    is_matIssue = BooleanField(default=False)
    matMatrix = CharField(max_length=10000, null=True, blank=True)

    def getEvaluationComparison(self, decision_user):
        criteria = self.decisioncriterion_variant_set.filter(is_criterion=True).order_by('name')
        alternatives = self.decisioncriterion_variant_set.filter(is_criterion=False).order_by('name')
        pairs = []
        # Criteria with respect to final goal
        exist_eval = Evaluation.objects.filter(decision_user=decision_user,
                                               crit_alter_left=criteria[0],
                                               crit_alter_right=criteria[1],
                                               considered_crit_alter=None).first()
        if exist_eval:
            pairs.append([None, itertools.combinations(criteria, 2), exist_eval.value])
        else:
            pairs.append([None, itertools.combinations(criteria, 2), None])
        # Variants with respect to criteria
        for criterion in criteria:
            exist_eval = Evaluation.objects.filter(decision_user=decision_user,
                                                   crit_alter_left=alternatives[0],
                                                   crit_alter_right=alternatives[1],
                                                   considered_crit_alter=criterion).first()
            if exist_eval:
                pairs.append([criterion, itertools.combinations(alternatives, 2), exist_eval.value])
            else:
                pairs.append([criterion, itertools.combinations(alternatives, 2), None])

        # Criteria with respect to alternatives
        for alt in alternatives:
            exist_eval = Evaluation.objects.filter(decision_user=decision_user,
                                                   crit_alter_left=criteria[0],
                                                   crit_alter_right=criteria[1],
                                                   considered_crit_alter=alt).first()
            if exist_eval:
                pairs.append([alt, itertools.combinations(criteria, 2), exist_eval.value])
            else:
                pairs.append([alt, itertools.combinations(criteria, 2), None])

        return pairs

    def getEvaluationVotes(self):
        decision_users = DecisionUser.objects.filter(decision_id=self.id, state=1)
        votes = Evaluation.objects.filter(decision_user__in=decision_users).order_by('order')
        return votes

    def getAnalysisData(self):
        data = ''
        if self.evalResult:
            results = numpy.round(numpy.fromstring(self.evalResult, dtype=numpy.float, sep=','), 2)
            results = numpy.multiply(results, 100)
            # order is necessary due to order of saved evalResult
            variants = self.decisioncriterion_variant_set.filter(is_criterion=False).order_by('name').values_list('name', flat=True)
            data = (results.tolist(), list(variants))
        return data

    def getAlternatives(self):
        variants = self.decisioncriterion_variant_set.filter(is_criterion=False).order_by('name').values_list('name', flat=True)
        results = numpy.round(numpy.fromstring(self.evalResult, dtype=numpy.float, sep=','), 2)
        results = numpy.multiply(results, 100)
        colors = ['rgba(24, 206, 15, 1)','rgba(44, 168, 255, 1)','rgba(255, 54, 54, 1)','rgba(249, 99, 50, 1)',
                  'rgba(75, 192, 192, 1)','rgba(255, 206, 86, 1)','rgba(54, 162, 235, 1)','rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)','rgba(255, 99, 132, 1)']
        resultArray = []
        for i, variant in enumerate(variants):
            resultArray.append([variant, results[i], colors[i]])
        return resultArray

    def getMatrix(self):
        matrixTxt = str(self.evalMatrix).replace("\n", "").replace("] [", "\n").replace("]]", "").replace("[[", "")
        matrix = numpy.loadtxt(StringIO(matrixTxt))
        return matrix

    def getCriteriaImportance(self):
        matrix = self.getMatrix()
        criteria_importance = []
        criteria_importance_labels = []
        criteria = self.decisioncriterion_variant_set.filter(is_criterion=True).order_by('name')
        for i, criterion in enumerate(criteria):
            criteria_importance_labels.append(criterion.name)
            criteria_importance.append(round(matrix[1 + i][0] * 100, 2))
        return (criteria_importance, criteria_importance_labels)

    def getAlternativesImportanceByCriteria(self):
        matrix = self.getMatrix()
        alt_by_criteria_importance = []
        criteria = self.decisioncriterion_variant_set.filter(is_criterion=True).order_by('name')
        criteria_len = criteria.count()
        alternatives = self.decisioncriterion_variant_set.filter(is_criterion=False).order_by('name')
        alternatives_importance_labels = list(alternatives.values_list('name', flat=True))
        for i, criterion in enumerate(criteria):
            alternatives_importance = []
            for j, alt in enumerate(alternatives):
                alternatives_importance.append(round(matrix[1+criteria_len+j][1+i]*100, 2))
            alt_by_criteria_importance.append([criterion.name, alternatives_importance, alternatives_importance_labels])
        return alt_by_criteria_importance

    def calculateGeometricMean(self, selectedVotes, matrix, i, j):
        # weighted geometric mean
        value = 1
        weights = 0
        if len(selectedVotes) > 0:
            for vote in selectedVotes:
                value *= vote.value ** (vote.decision_user.weight / 100.0)
                weights += (vote.decision_user.weight / 100.0)
            value = value ** (1 / weights)
            matrix[i, j] = value
            matrix[j, i] = 1 / value

    def calcWeightMatrix(self, critAlterArray, votes, consideredCritAlter=None):
        # create matrix for each criterion or alternative
        weightMatrix = numpy.eye(len(critAlterArray))
        for i, left in enumerate(critAlterArray):
            for j, right in enumerate(critAlterArray):
                if weightMatrix[i, j] == 0:
                    selectedVotes = votes.filter(crit_alter_left=left,
                                                 crit_alter_right=right,
                                                 considered_crit_alter=consideredCritAlter)
                    # weighted geometric mean
                    self.calculateGeometricMean(selectedVotes, weightMatrix, i, j)

        return weightMatrix

    def calcWeightEigenVector(self, critAlterArray, votes, consideredCritAlter=None):
        # STEP 1
        # create matrix for each criterion or alternative
        weightMatrix = self.calcWeightMatrix(critAlterArray, votes, consideredCritAlter)

        # STEP 2
        # get eigenvalues, eigenvector from every matrix
        eigenvalues, eigenvector = numpy.linalg.eig(weightMatrix)
        warnings.simplefilter("ignore", numpy.ComplexWarning)

        # STEP 3
        # calculate weight vector from STEP 2
        maxindex = numpy.argmax(eigenvalues)
        eigenvector = numpy.float32(eigenvector)
        weight = eigenvector[:, maxindex]  # extract vector from eigenvector with max value in eigenvalues
        # weight.tolist()  # convert array(numpy)  to vector

        # STEP 4
        # transform eigen vector into normalized eigen vector
        weight = [w / sum(weight) for w in weight]

        return weight

    def evaluateDecisionAnalysis(self):
        votes = self.getEvaluationVotes()
        criteria = self.decisioncriterion_variant_set.filter(is_criterion=True).order_by('name')
        variants = self.decisioncriterion_variant_set.filter(is_criterion=False).order_by('name')
        criteriaLen = len(criteria)
        variantsLen = len(variants)
        matrix = numpy.eye(1 + criteriaLen + variantsLen) # create N-size matrix with ones on the diagonal
        for i, weight in enumerate(self.calcWeightEigenVector(criteria, votes)):
            matrix[i + 1, 0] = weight
        for j, criterion in enumerate(criteria):
            for i, weight in enumerate(self.calcWeightEigenVector(variants, votes, criterion)):
                matrix[1 + criteriaLen + i, j + 1] = weight
        for j, variant in enumerate(variants):
            for i, weight in enumerate(self.calcWeightEigenVector(criteria, votes, variant)):
                matrix[1 + i, j + 1 + criteriaLen] = weight
        self.evalMatrix = matrix

        # STEP 5 calculate weighted sum of evaluated criteria with their weights
        i = 1
        diff = True
        weighted_sum = numpy.empty(0)
        weighted_sum_before = None
        while diff:
            supermatrix_pow = numpy.linalg.matrix_power(matrix, 2 * i)
            for j in range(0, variantsLen):
                weighted_sum = numpy.append(weighted_sum, supermatrix_pow.item(1 + j + variantsLen, 0))
            weighted_sum = weighted_sum / numpy.linalg.norm(weighted_sum, 1)
            if weighted_sum_before is not None:
                difference = weighted_sum - weighted_sum_before
                newDiff = False
                for k in difference:
                    if abs(k) > self.MATRIX_PRECISION:
                        newDiff = True
                diff = newDiff
            weighted_sum_before = weighted_sum
            weighted_sum = numpy.empty(0)
            i += 1
        self.evalResult = ",".join(str(x) for x in weighted_sum_before)
        self.save()

    def setMathematicalMatrix(self, data):
        dimensionX = int(data.get('dim_x'))
        dimensionY = int(data.get('dim_y'))
        matrix = []
        i = 0
        for input in data:
            if input.startswith('matrix'):
                matrix = numpy.insert(matrix, i, data[input])
                i += 1
        self.matMatrix = numpy.reshape(matrix, (dimensionY, dimensionX))
        self.save()

    def getMathematicalMatrix(self):
        matrixTxt = str(self.matMatrix).replace("\n", "").replace("] [", "\n").replace("]]", "").replace("[[", "")
        matrix = numpy.loadtxt(StringIO(matrixTxt))
        return matrix

    def calculateMathematicalParameters(self):
        matrix = self.getMathematicalMatrix()
        finalResult = []
        calcMean = numpy.mean(matrix, axis=0)
        calcDiff = numpy.divide(matrix, calcMean)
        computableCriteria = self.decisioncriterion_variant_set.filter(is_criterion=True,
                            is_computable=True).order_by('name').values_list('computed_value', flat=True)
        alternativesImportance = self.getAlternativesImportanceByCriteria()
        for i,criterion in enumerate(alternativesImportance):
            importance = criterion[1]
            values = calcDiff[:,i]
            if computableCriteria[i] == 0:
                values = numpy.divide(1, values)
            result = numpy.multiply(importance, values).tolist()
            if not finalResult:
                finalResult = result
            finalResult = [x + y for x, y in zip(finalResult, result)]

        return [finalResult,alternativesImportance[0][2]]

    def __str__(self):
        return self.name


class DecisionCriterion_Variant(Model):
    decision = ForeignKey('DecisionProject', on_delete=CASCADE)
    name = CharField(max_length=200)
    description = CharField(max_length=500, blank=True)
    image = ImageField(null=True, blank=True, upload_to="img/decisions", default='no-image.jpg')
    is_criterion = BooleanField(default=None)

    is_computable = BooleanField(default=False)
    COMPUTED_CHOICES = (
        (0, u'Minimize'),
        (1, u'Maximize'),
    )
    computed_value = IntegerField(choices=COMPUTED_CHOICES, default=0)

    def __str__(self):
        if self.is_criterion:
            return 'Criterion - ' + self.name
        else:
            return 'Alternative - ' + self.name


class DecisionUser(Model):
    ROOKIE = 20
    TRAINEE = 40
    EDUCATED = 60
    PROFESSIONAL = 80
    EXPERT = 100
    WEIGHT_CHOICES = (
        (ROOKIE, u'Rookie'),
        (TRAINEE, u'Trainee'),
        (EDUCATED, u'Educated'),
        (PROFESSIONAL, u'Professional'),
        (EXPERT, u'Expert'),
    )
    weight = IntegerField(choices=WEIGHT_CHOICES , default=EXPERT)
    decision = ForeignKey('DecisionProject', on_delete=CASCADE)
    user = ForeignKey('CustomUser', on_delete=DO_NOTHING)

    SENT = 0
    COMPLETED = 1
    STATE_CHOICES = (
        (SENT, u'Evaluation not completed'),
        (COMPLETED, u'Evaluation completed'),
    )
    state = IntegerField(choices=STATE_CHOICES, default=SENT, blank=True)

    class Meta:
        unique_together = ('decision', 'user')

    def __str__(self):
        return " - ".join([self.decision.name, self.user.username])


class Evaluation(Model):
    LEFT_VERYSTRONG = 9
    LEFT_STRONG = 7
    LEFT = 5
    LEFT_WEAK = 3
    SAME = 1
    RIGHT_WEAK = 0.333333
    RIGHT = 0.2
    RIGHT_STRONG = 0.142857
    RIGHT_VERYSTRONG = 0.111111
    VOTE_CHOICES = (
        (LEFT_VERYSTRONG, u'Definitely left'),
        (LEFT_STRONG, u'Left'),
        (LEFT, u'Assume left'),
        (LEFT_WEAK, u'Maybe left'),
        (SAME, u'Same'),
        (RIGHT_WEAK, u'Maybe right'),
        (RIGHT, u'Assume right'),
        (RIGHT_STRONG, u'Right'),
        (RIGHT_VERYSTRONG, u'Definitely right'),
    )
    order = IntegerField()
    decision_user = ForeignKey('DecisionUser', on_delete=CASCADE)
    value = FloatField(choices=VOTE_CHOICES, default=SAME)

    considered_crit_alter = ForeignKey('DecisionCriterion_Variant', on_delete=DO_NOTHING, related_name='crit_alter',
                                       default=None, null=True, blank=True)
    crit_alter_left = ForeignKey('DecisionCriterion_Variant', on_delete=DO_NOTHING, related_name='crit_alter_left',
                                 default=None, null=True)
    crit_alter_right = ForeignKey('DecisionCriterion_Variant', on_delete=DO_NOTHING, related_name='crit_alter_right',
                                  default=None, null=True)

    def __str__(self):
        if self.considered_crit_alter:
            return self.crit_alter_left.name + ' vs ' + self.crit_alter_right.name + ' (based on ' + self.considered_crit_alter.name + ')'
        return self.crit_alter_left.name + ' vs ' + self.crit_alter_right.name


