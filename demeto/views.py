from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import DetailView
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import CreateView, UpdateView

from demeto import settings
from demeto.forms import CustomUserForm, DecisionProjectForm, AlternativesFormSet, CriteriaFormSet, UsersFormSet, \
    CustomUserEditForm
from demeto.mixins import RestrictionTemplateMixin, CreateFormsetView
from demeto.models import CustomUser, DecisionProject, DecisionUser, Evaluation, \
    DecisionCriterion_Variant


class Dashboard(LoginRequiredMixin, TemplateView):
    template_name = "dashboard.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        decision_ids = DecisionUser.objects.filter(user=self.request.user).values_list('decision', flat=True)
        context['projects'] = DecisionProject.objects.filter(pk__in=decision_ids)
        return context


class UserCreateView(CreateView):
    template_name = "registration/registration.html"
    model = CustomUser
    form_class = CustomUserForm
    success_url = "/"

    def form_valid(self, form):
        # save the new user first
        form.save()
        username = self.request.POST['username']
        password = self.request.POST['password1']
        # authenticate user then login
        user = authenticate(username=username, password=password)
        if user is not None:
            login(self.request, user)
            return HttpResponseRedirect(settings.LOGIN_REDIRECT_URL)

        return super(UserCreateView, self).form_valid(form)


class LogOutView(LoginRequiredMixin, TemplateView):
    template_name = 'registration/logout.html'

    def get(self, request):
        logout(request)
        return super(LogOutView, self).get(request)


class ProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'profile.html'
    model = CustomUser
    form_class = CustomUserEditForm

    def dispatch(self, request, *args, **kwargs):
        if request.user.id == kwargs['pk']:
            return super().dispatch(request, *args, **kwargs)
        else:
            return HttpResponseRedirect(reverse('profile', args=[request.user.id]))

    def form_valid(self, form):
        self.object = form.save()
        return self.render_to_response(self.get_context_data(form=form, message=True))


class DecisionAddView(LoginRequiredMixin, CreateView):
    template_name = 'decision/project-add.html'
    model = DecisionProject
    form_class = DecisionProjectForm

    def form_valid(self, form):
        """
        Called if all forms are valid. and then redirects to a
        alternatives page.
        """
        self.object = form.save(commit=False)
        self.object.creator = self.request.user
        self.object.save()
        DecisionUser.objects.create(decision_id=self.object.id, user_id=self.object.creator.id)
        return HttpResponseRedirect(reverse('alter-decision', args=[self.object.id]))

class DecisionAlternativesView(CreateFormsetView):
    template_name = 'decision/alternatives.html'
    model = DecisionCriterion_Variant
    necessary_state = DecisionProject.NEW

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates blank versions of the form
        and its inline formsets.
        """
        super().get(request, *args, **kwargs)
        form = AlternativesFormSet(queryset=DecisionCriterion_Variant.objects.filter(decision=self.kwargs['pk'],
                                                                                     is_criterion=False))
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance and its inline
        formsets with the passed POST variables and then checking them for
        validity.
        """
        super().post(request, *args, **kwargs)
        form = AlternativesFormSet(self.request.POST, self.request.FILES)
        if (form.is_valid()):
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        decision_id = self.kwargs['pk']
        decision = DecisionProject.objects.get(pk=decision_id)
        for alternative in form:
            cd = alternative.cleaned_data
            alter = cd['id']
            if alter:
                if cd['DELETE']:
                    alter.delete()
                else:
                    alter.name = cd.get('name')
                    alter.description = cd.get('description')
                    alter.image = cd.get('image') if cd.get('image') else 'no-image.jpg'
                    alter.save()
            elif 'name' in cd:
                name = cd.get('name')
                description = cd.get('description')
                image = cd.get('image')
                alter = DecisionCriterion_Variant(name = name,
                                                  description = description,
                                                  image = image,
                                                  decision = decision,
                                                  is_criterion= False)
                alter.save()
        if decision.state < DecisionProject.STAGE_ALTERNATIVES:
            decision.state = DecisionProject.STAGE_ALTERNATIVES
            decision.save()

        return HttpResponseRedirect(reverse('crit-decision', args=[decision_id]))


class DecisionCriteriaView(CreateFormsetView):
    template_name = 'decision/criteria.html'
    model = DecisionCriterion_Variant
    necessary_state = DecisionProject.STAGE_ALTERNATIVES

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        form = CriteriaFormSet(queryset=DecisionCriterion_Variant.objects.filter(decision=self.kwargs['pk'],
                                                                                 is_criterion=True))
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)
        form = CriteriaFormSet(self.request.POST, self.request.FILES)
        if (form.is_valid()):
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_invalid(self, form):
        return self.render_to_response(
            self.get_context_data(form=form))

    def form_valid(self, form):
        decision_id = self.kwargs['pk']
        decision = DecisionProject.objects.get(pk=decision_id)
        for criteria in form:
            cd = criteria.cleaned_data
            crit = cd['id']
            if crit:
                if cd['DELETE']:
                    crit.delete()
                else:
                    crit.name = cd.get('name')
                    crit.description = cd.get('description')
                    crit.is_computable = cd.get('is_computable')
                    crit.computed_value = cd.get('computed_value')
                    crit.image = cd.get('image') if cd.get('image') else 'no-image.jpg'
                    crit.save()
            elif 'name' in cd:
                name = cd.get('name')
                description = cd.get('description')
                is_computable = cd.get('is_computable')
                computed_value = cd.get('computed_value')
                image = cd.get('image')
                criterion = DecisionCriterion_Variant(name = name, description = description,
                                                      image = image, decision = decision,
                                                      is_criterion=True, is_computable=is_computable,
                                                      computed_value=computed_value)
                criterion.save()

        if decision.state < DecisionProject.STAGE_CRITERIA:
            decision.state = DecisionProject.STAGE_CRITERIA
        decision.is_matIssue = False
        decision.save()
        if not decision.decisioncriterion_variant_set.filter(is_criterion=True,is_computable=False):
            decision.is_matIssue = True
            decision.save()
            return HttpResponseRedirect(reverse('param-decision', args=[decision_id]))

        return HttpResponseRedirect(reverse('users-decision', args=[decision_id]))


class DecisionParametersView(TemplateView, LoginRequiredMixin, RestrictionTemplateMixin):
    template_name = 'decision/parameters.html'
    necessary_state = DecisionProject.STAGE_CRITERIA

    def post(self, request, *args, **kwargs):
        project = DecisionProject.objects.get(pk=self.kwargs['pk'])
        project.setMathematicalMatrix(self.request.POST)
        return self.render_to_response(self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        project = DecisionProject.objects.get(pk=self.kwargs['pk'])
        context['creator_id'] = project.creator_id
        context['criteria'] = DecisionCriterion_Variant.objects.filter(decision=self.kwargs['pk'],
                                                                       is_criterion=True).order_by('name')
        context['alternatives'] = DecisionCriterion_Variant.objects.filter(decision=self.kwargs['pk'],
                                                                           is_criterion=False).order_by('name')
        if project.matMatrix:
            context['matrix'] = project.getMathematicalMatrix().tolist()
        return context


class DecisionUsersView(CreateFormsetView):
    template_name = 'decision/users.html'
    model = DecisionUser
    necessary_state = DecisionProject.STAGE_CRITERIA

    def get(self, request, *args, **kwargs):
        super().get(request, *args, **kwargs)
        form = UsersFormSet(queryset=DecisionUser.objects.filter(decision=self.kwargs['pk']))
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)
        form = UsersFormSet(self.request.POST)
        if (form.is_valid()):
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        decision_id = self.kwargs['pk']
        decision = DecisionProject.objects.get(pk=decision_id)
        for users in form:
            cd = users.cleaned_data
            dec_user = DecisionUser.objects.filter(user=cd['user'], decision_id=decision_id)
            if not dec_user.exists():
                if 'user' in cd and 'weight' in cd:
                    user = cd.get('user')
                    weight = cd.get('weight')
                    new_user = DecisionUser(user=user,
                                            decision_id=decision_id,
                                            weight=weight,)
                    new_user.save()
            elif cd['DELETE'] and dec_user.first().user_id != decision.creator_id:
                dec_user.delete()

        if decision.state < DecisionProject.STAGE_USERS:
            decision.state = DecisionProject.STAGE_USERS
            decision.save()

        return HttpResponseRedirect(reverse('eval-decision', args=[decision_id]))

class DecisionEvalView(LoginRequiredMixin, RestrictionTemplateMixin, TemplateView):
    template_name = 'decision/evaluation.html'
    necessary_state = DecisionProject.STAGE_USERS

    def get_context_data(self, **kwargs):
        context = super(DecisionEvalView, self).get_context_data(**kwargs)
        decision = DecisionProject.objects.get(pk=kwargs['pk'])
        context['decision'] = decision
        context['choices'] = Evaluation.VOTE_CHOICES

        return context

class DecisionDetailView(LoginRequiredMixin, RestrictionTemplateMixin, DetailView):
    template_name = 'decision/detail.html'
    model = DecisionProject
    necessary_state = DecisionProject.STAGE_EVALUATION

    def get_context_data(self, **kwargs):
        context = super(DecisionDetailView, self).get_context_data(**kwargs)
        context['results'] = self.object.getAlternatives()
        context['completed_users'] = self.object.decisionuser_set.filter(state=True).count()
        alter_values = self.object.getAnalysisData()[0]
        alternatives = self.object.decisioncriterion_variant_set.filter(is_criterion=False).order_by('name')
        context['best_alternative'] = alternatives[alter_values.index(max(alter_values))].name
        context['criteria_count'] = self.object.decisioncriterion_variant_set.filter(is_criterion=True).order_by('name').count()
        context['alternatives_count'] = alternatives.count()
        context['criteria_importance'] = self.object.getCriteriaImportance()
        context['alternatives_importance'] = self.object.getAlternativesImportanceByCriteria()
        if self.object.is_matIssue:
            context['score'] = self.object.calculateMathematicalParameters()

        return context

@login_required
def ajaxAddEvaluation(request):
    if request.is_ajax() and request.method == 'POST' and request.POST.get('value'):
        decision_id = request.POST.get('decision_id')
        vote_left_id = request.POST.get('vote_left_id')
        vote_right_id = request.POST.get('vote_right_id')
        considered_id = request.POST.get('considered_id')
        order = request.POST.get('order')
        value = request.POST.get('value')

        decisionUser = DecisionUser.objects.get(decision_id=decision_id, user=request.user)
        consideredCrit_Var = DecisionCriterion_Variant.objects.get(pk=considered_id) if considered_id else None
        voteLeftCrit_Var = DecisionCriterion_Variant.objects.get(pk=vote_left_id)
        voteRightCrit_Var = DecisionCriterion_Variant.objects.get(pk=vote_right_id)
        eval = Evaluation.objects.filter(decision_user=decisionUser,
                                         considered_crit_alter=consideredCrit_Var,
                                         crit_alter_left= voteLeftCrit_Var,
                                         crit_alter_right=voteRightCrit_Var,)
        if not eval:
            eval = Evaluation(decision_user=decisionUser,
                              considered_crit_alter=consideredCrit_Var,
                              crit_alter_left= voteLeftCrit_Var,
                              crit_alter_right=voteRightCrit_Var,
                              value=value,
                              order=order,)
        else:
            eval = eval.first()
            eval.value = value
        eval.save()

        return JsonResponse({'response': 'Vote saved'}, safe=False)

@login_required
def ajaxCheckVotesEvaluation(request):
    if request.is_ajax() and request.method == 'POST':
        decision_id = request.POST.get('decision_id')
        decision = DecisionProject.objects.get(pk=decision_id)
        decision_user = DecisionUser.objects.get(decision_id=decision_id, user=request.user)
        votesCount = decision_user.evaluation_set.select_related().count()
        if int(request.POST.get('votes_count')) == votesCount:
            decision.state = DecisionProject.STAGE_EVALUATION
            decision.save()
            decision_user.state = DecisionUser.COMPLETED
            decision_user.save()
            decision.evaluateDecisionAnalysis()
            return JsonResponse({'response': 'valid'}, safe=False)

        return JsonResponse({'response': 'error'}, safe=False)