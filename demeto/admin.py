from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from demeto.models import DecisionProject, DecisionCriterion_Variant, DecisionUser, CustomUser, Evaluation


class DecisionCriterion_VariantInline(admin.TabularInline):
    model = DecisionCriterion_Variant
    extra = 0


class DecisionAdmin(admin.ModelAdmin):
    inlines = [DecisionCriterion_VariantInline]


class EvaluationInline(admin.TabularInline):
    model = Evaluation
    extra = 0


class DecisionUserAdmin(admin.ModelAdmin):
    list_filter = ('decision',)
    inlines = [EvaluationInline]

    def __str__(self):
        return 'Evaluations'


class CustomUserAdmin(UserAdmin):
    model = CustomUser
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('image',)}),
    )


# Register your models here.
admin.site.register(DecisionUser, DecisionUserAdmin)
admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(DecisionProject, DecisionAdmin)