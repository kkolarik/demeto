from django import template
from django.urls import reverse

from demeto.models import DecisionProject, DecisionUser

register = template.Library()

@register.simple_tag(takes_context=True)
def url_active(context, viewname, id=False, state=False):
    if viewname :
        request = context['request']
        current_path = request.path
        result = ''
        if id:
            compare_path = reverse(str(viewname), args=[id])
            if state and DecisionProject.objects.get(pk=id).state >= state:
                result = 'done '
        else:
            compare_path = reverse(str(viewname))
        if compare_path == current_path:
            result += 'active'
        return result
    return ''

@register.simple_tag
def url_state (id, state):
    if DecisionProject.objects.get(pk=id).state + 1 < state:
        return 'not-active'
    return ''

@register.simple_tag
def evaluation_comparsion (id, user):
    user = DecisionUser.objects.get(user=user, decision_id=id)
    data = DecisionProject.objects.get(pk=id).getEvaluationComparison(user)
    return data

@register.simple_tag
def enable_parameters(id):
    return DecisionProject.objects.get(pk=id).is_matIssue

@register.filter(name='zip')
def zip_lists(a, b):
    return zip(a, b)