(function () {
    isWindows = navigator.platform.indexOf('Win') > -1 ? true : false;
    if (isWindows) {
        // if we are on windows OS we activate the perfectScrollbar function
        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

        $('html').addClass('perfect-scrollbar-on');
    } else {
        $('html').addClass('perfect-scrollbar-off');
    }
})();

$(document).ready(function () {
    if ($('.full-screen-map').length == 0 && $('.bd-docs').length == 0) {
        // On click navbar-collapse the menu will be white not transparent
        $('.collapse').on('show.bs.collapse', function () {
            $(this).closest('.navbar').removeClass('navbar-transparent').addClass('bg-white');
        }).on('hide.bs.collapse', function () {
            $(this).closest('.navbar').addClass('navbar-transparent').removeClass('bg-white');
        });
    }
    nowuiDashboard.initMinimizeSidebar();
    $navbar = $('.navbar[color-on-scroll]');
    scroll_distance = $navbar.attr('color-on-scroll') || 500;
    // Check if we have the class "navbar-color-on-scroll" then add the function to remove the class "navbar-transparent" so it will transform to a plain color.
    if ($('.navbar[color-on-scroll]').length != 0) {
        nowuiDashboard.checkScrollForTransparentNavbar();
        $(window).on('scroll', nowuiDashboard.checkScrollForTransparentNavbar);
    }
    $('.form-control').on("focus", function () {
        $(this).parent('.input-group').addClass("input-group-focus");
    }).on("blur", function () {
        $(this).parent(".input-group").removeClass("input-group-focus");
    });
    // Activate bootstrapSwitch
    $('.bootstrap-switch').each(function () {
        $this = $(this);
        data_on_label = $this.data('on-label') || '';
        data_off_label = $this.data('off-label') || '';
        $this.bootstrapSwitch({
            onText: data_on_label,
            offText: data_off_label
        });
    });
});

$(document).on('click', '.navbar-toggle', function () {
    $toggle = $(this);
    if (nowuiDashboard.misc.navbar_menu_visible == 1) {
        $('html').removeClass('nav-open');
        nowuiDashboard.misc.navbar_menu_visible = 0;
        setTimeout(function () {
            $toggle.removeClass('toggled');
            $('#bodyClick').remove();
        }, 550);
    } else {
        setTimeout(function () {
            $toggle.addClass('toggled');
        }, 580);
        div = '<div id="bodyClick"></div>';
        $(div).appendTo('body').click(function () {
            $('html').removeClass('nav-open');
            nowuiDashboard.misc.navbar_menu_visible = 0;
            setTimeout(function () {
                $toggle.removeClass('toggled');
                $('#bodyClick').remove();
            }, 550);
        });
        $('html').addClass('nav-open');
        nowuiDashboard.misc.navbar_menu_visible = 1;
    }
});

$(window).resize(function () {
  // reset the seq for charts drawing animations
  seq = seq2 = 0;
  if ($('.full-screen-map').length == 0 && $('.bd-docs').length == 0) {
    $navbar = $('.navbar');
    isExpanded = $('.navbar').find('[data-toggle="collapse"]').attr("aria-expanded");
    if ($navbar.hasClass('bg-white') && $(window).width() > 991) {
      $navbar.removeClass('bg-white').addClass('navbar-transparent');
    } else if ($navbar.hasClass('navbar-transparent') && $(window).width() < 991 && isExpanded != "false") {
      $navbar.addClass('bg-white').removeClass('navbar-transparent');
    }
  }
});

nowuiDashboard = {
    misc: {
        navbar_menu_visible: 0
    },
    initMinimizeSidebar: function initMinimizeSidebar() {
        if ($('.sidebar-mini').length != 0) {
            sidebar_mini_active = true;
        }
        $('#minimizeSidebar').click(function () {
            var $btn = $(this);
            if (sidebar_mini_active == true) {
                $('body').removeClass('sidebar-mini');
                sidebar_mini_active = false;
                nowuiDashboard.showSidebarMessage('Sidebar mini deactivated...');
            } else {
                $('body').addClass('sidebar-mini');
                sidebar_mini_active = true;
                nowuiDashboard.showSidebarMessage('Sidebar mini activated...');
            }
            // we simulate the window Resize so the charts will get updated in realtime.
            var simulateWindowResize = setInterval(function () {
                window.dispatchEvent(new Event('resize'));
            }, 180);

            // we stop the simulation of Window Resize after the animations are completed
            setTimeout(function () {
                clearInterval(simulateWindowResize);
            }, 1000);
        });
    },

    showSidebarMessage: function showSidebarMessage(message) {
        try {
            $.notify({
                icon: "now-ui-icons ui-1_bell-53",
                message: message
            }, {
                type: 'info',
                timer: 4000,
                placement: {
                    from: 'top',
                    align: 'right'
                }
            });
        } catch (e) {
            console.log('Notify library is missing, please make sure you have the notifications library added.');
        }
    }
};

hexToRGB = function hexToRGB(hex, alpha) {
    var r = parseInt(hex.slice(1, 3), 16),
        g = parseInt(hex.slice(3, 5), 16),
        b = parseInt(hex.slice(5, 7), 16);
    if (alpha) {
        return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
    } else {
        return "rgb(" + r + ", " + g + ", " + b + ")";
    }
};

/** end of now ui template js **/
demeto = {
    initDashboardGraph: function (decision_id, data, labels) {
        var ctx = document.getElementById("chart-analysis-" + decision_id).getContext("2d");
		var myDoughnutChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: labels,
                datasets: [{
                    data: data,
                    backgroundColor: [
                        'rgba(24, 206, 15, 1)',
                        'rgba(44, 168, 255, 1)',
                        'rgba(255, 54, 54, 1)',
                        'rgba(249, 99, 50, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 99, 132, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                responsive: true,
                animation: {
					animateScale: true,
					animateRotate: true
				},
				tooltips: {
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return data.labels[tooltipItems.index] +': ' + data.datasets[0].data[tooltipItems.index] + '%';
                        }
                    }
                }
            }
        });
    },
    initCriteriaGraph: function (data) {
        var colors = ['rgba(24, 206, 15, 1)','rgba(44, 168, 255, 1)', 'rgba(255, 54, 54, 1)','rgba(249, 99, 50, 1)',
                      'rgba(75, 192, 192, 1)','rgba(255, 206, 86, 1)', 'rgba(54, 162, 235, 1)','rgba(153, 102, 255, 1)'];
        var labels = data[0][2];
        var datasets = [];
        for (var key in data) {
            datasets.push({
                label: data[key][0],
                data: data[key][1],
                backgroundColor: colors[key]
            });
        }
        var preparedData = {
            labels: labels,
            datasets: datasets
        };

        var ctx = document.getElementById("criteria-score").getContext("2d");
		var myScoreChart = new Chart(ctx, {
            type: 'bar',
            data: preparedData,
            options: {
                 scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'percentage'
                        }
                    }]
                },
                legend: {
                    display: true
                },
                responsive: true,
                tooltips: {
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel + '%';
                        }
                    }
                }
            }
        });
    },
    initScoreboardGraph: function (data) {
        var colors = ['rgba(44, 168, 255, 1)'];
        var labels = data[1];
        var datasets = [{
                label: 'Score',
                data: data[0],
                backgroundColor: colors[0]
        }];
        var preparedData = {
            labels: labels,
            datasets: datasets
        };

        var ctx = document.getElementById("chart-score").getContext("2d");
		var myScoreChart = new Chart(ctx, {
            type: 'bar',
            data: preparedData,
            options: {
                 scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Score'
                        }
                    }]
                },
                legend: {
                    display: true
                },
                responsive: true,
                tooltips: {
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return data.datasets[tooltipItems.datasetIndex].label +': ' + tooltipItems.yLabel.toFixed(1);
                        }
                    }
                }
            }
        });
    }
};